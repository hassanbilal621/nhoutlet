<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Api extends CI_Controller
{


    public function __construct()
    {
      parent::__construct();
      $this->load->database();
      $this->load->library('form_validation');

    }

    public function get_products($limit){

        $query = $this->db->get('product', $limit);
        $products = $query->result_array(); // for single row_array and for multiple result_array

        $data['products'] =  $products;

        print_r(json_encode($data));

        
    }

    public function get_products_from($startfrom,$limit){
        $this->db->order_by("product_id", "asc");
        $this->db->where('product.product_id >', $startfrom);
        $query = $this->db->get('product', $limit);
        $products = $query->result_array(); // for single row_array and for multiple result_array

        $data['products'] =  $products;

        print_r(json_encode($data));
        
    }


    public function get_products_cat_from($catid, $startfrom,$limit){
        $this->db->order_by("product_id", "asc");
        $this->db->where('category', $catid);
        $this->db->where('product.product_id >', $startfrom);
        $query = $this->db->get('product', $limit);
        $products = $query->result_array(); // for single row_array and for multiple result_array

        $data['products'] =  $products;

        print_r(json_encode($data));
        
    }



    public function get_products_by_catid($catid, $limit){


        $this->db->where('category', $catid);
        $query = $this->db->get('product', $limit);
        $products = $query->result_array(); // for single row_array and for multiple result_array

        $data['products'] =  $products;

        print_r(json_encode($data));

        
    }

    public function get_categories(){

        $query = $this->db->get('category');
        $category = $query->result_array(); // for single row_array and for multiple result_array

        $data['categories'] =  $category;

        print_r(json_encode($data));

        
    }


    public function get_subcategories($catid){


        $this->db->where('category', $catid);

        $query = $this->db->get('sub_category');
        $category = $query->result_array(); // for single row_array and for multiple result_array

        $data['subcategories'] =  $category;

        print_r(json_encode($data));

        
    }



    // public function get_categories_products(){

    //     $query = $this->db->get('category');
    //     $categories = $query->result_array(); // for single row_array and for multiple result_array

    //     print_r($categories);

    //     foreach($categories as $category):


    //         print_r($category);
    //         $data['categories'] =  array_push($category);

    //     endforeach;



    //     print_r(json_encode($data));

        
    // }

    public function get_brand(){

        $query = $this->db->get('product');
        $brand = $query->result_array(); // for single row_array and for multiple result_array

        $data['brands'] =  $brand;

        print_r(json_encode($data));

        
    }


    public function get_sub_category(){

        $query = $this->db->get('sub_category');
        $sub_category = $query->result_array(); // for single row_array and for multiple result_array

        $data['sub_category'] =  $sub_category;

        print_r(json_encode($data));

        
    }

    public function post_login(){


        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
        } else {
            $signin_data = $this->db->get_where('user', array(
                'email' => $this->input->post('email'),
                'password' => sha1($this->input->post('password'))
            ));
            if ($signin_data->num_rows() > 0) {
                foreach ($signin_data->result_array() as $row) {
                    // $this->session->set_userdata('user_login', 'yes');
                    // $this->session->set_userdata('user_id', $row['user_id']);
                    // $this->session->set_userdata('user_name', $row['username']);
                    $this->session->set_flashdata('alert', 'successful_signin');
                    $this->db->where('user_id', $row['user_id']);
                    $this->db->update('user', array(
                        'last_login' => time()
                    ));
                    echo $row['user_id'];
                }
            } else {
                echo 'failed';
            }
        }
  

    }

    public function post_register(){

        $this->form_validation->set_rules('username', 'First Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|is_unique[user.email]|valid_email', array('required' => 'You have not provided %s.', 'is_unique' => 'This %s already exists.'));
        $this->form_validation->set_rules('password1', 'Password', 'required|matches[password2]');
        $this->form_validation->set_rules('password2', 'Confirm Password', 'required');
        $this->form_validation->set_rules('address1', 'Address Line 1', 'required');
        $this->form_validation->set_rules('phone', 'Phone', 'required');
        $this->form_validation->set_rules('surname', 'Last Name', 'required');
        $this->form_validation->set_rules('zip', 'ZIP', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');

        if ($this->form_validation->run() == FALSE) {
            echo validation_errors();
        } else {

            $data['username'] = $this->input->post('username');
            $data['email'] = $this->input->post('email');
            $data['address1'] = $this->input->post('address1');
            $data['address2'] = $this->input->post('address2');
            $data['phone'] = $this->input->post('phone');
            $data['surname'] = $this->input->post('surname');
            $data['zip'] = $this->input->post('zip');
            $data['city'] = $this->input->post('city');
            $data['state'] = $this->input->post('state');
            $data['country'] = $this->input->post('country');
            $data['langlat'] = '';
            $data['wishlist'] = '[]';
            $data['package_info'] = '[]';
            $data['product_upload'] = $this->db->get_where('package', array('package_id' => 1))->row()->upload_amount;
            $data['creation_date'] = time();
            if ($this->input->post('password1') == $this->input->post('password2')) {
                $password = $this->input->post('password1');
                $data['password'] = sha1($password);
                $this->db->insert('user', $data);
                $msg = 'done';
                if ($this->email_model->account_opening('user', $data['email'], $password) == false) {
                    $msg = 'done_but_not_sent';
                } else {
                    $msg = 'done_and_sent';
                }
                echo $msg;
            }else{

                echo 'Password Incorrect';
            }

        }




    }






}